

public class car {
	public int odometer;
	public String brand;
	public String color;
	public int gear;
	
	public car(int o , String b , String c) {
		odometer = o;
		brand = b;
		color = c;
		gear = 0;
	}
	
	public car( String b , String c) {
		odometer = 0;
		brand = b;
		color = c;
		gear = 0;
	}
	public void incrementGear() {
		if (gear == 5) {
			System.out.println("Last gear is 5. You are on the border.");
		}
		else {
			gear = gear + 1 ;
		}
	}
	public void decrementtGear() {
		if (gear == 0) {
			System.out.println("First gear is 1. You are on the border.");
		}
		else {
			gear = gear - 1 ;
		}
	}
	public void drive(int h,int k) {
		odometer = odometer + h*k;
	}

	public int getOdometer() {
		return odometer;
	}

	public void setOdometer(int odometer) {
		this.odometer = odometer;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public int getGear() {
		return gear;
	}

	public void setGear(int gear) {
		this.gear = gear;
	}
	public void display() {
		System.out.println("Car's odometer = " + odometer );
		System.out.println("Car's brand = " + brand );
		System.out.println("Car's color = " + color );
		System.out.println("Car's gear  = " + gear );
	}
	
	
	
	
	
	
	
	
}
